<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="min-h-screen flex">
    
    <?php
    function read()
    {
        $cookie_name = "cart";
        if (!isset($_COOKIE[$cookie_name])) {
            echo "<div class='m-auto bg-white shadow-xl px-3 py-2 flex flex-col items-center'><h1 class='capitalize font-semibold text-xl'>cart kosong</h1><img src='../assets/img1-.png' class='w-[25rem]' /></div>";
        } else {
            $cart = json_decode($_COOKIE[$cookie_name], true);
            echo "<div class='m-auto bg-slate-200 shadow-xl px-3 py-5 rounded-lg w-full max-w-lg'><table class='w-full border-separate border-spacing-7' border=2>";
            echo '<h3 class="text-xl font-semibold">Cart Anda</h3>';
            echo "<tr><td>No</td><td>ID</td><td> Judul </td><td>Aksi</td></tr>";
            $i = 0;
            foreach ($cart as $row) {
                echo "<tr><td>$i</td><td>$row[0]</td><td> $row[1] </td><td><a class='bg-blue-900 hover:bg-blue-950 text-white px-3 py-1 rounded-md' href='./pinjam.php?fitur=delete&idbuku=$i'>Hapus</td></tr>";
                $i++;
            }
            echo "</table>";
            echo "<div class='flex gap-x-1 mt-2'><a class='bg-blue-900 hover:bg-blue-950 text-white px-3 py-1 rounded-md' href='../fitur.php'>CARI</a> <br>";
            echo "<a class='bg-blue-900 hover:bg-blue-950 text-white px-3 py-1 rounded-md' href='pinjam.php?fitur=save'>SIMPAN</a></div></div>";
        }
    }
    ?>
</body>

</html>