<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cari</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <?php
    function cari($keyword)
    {
        $link = mysqli_connect(
            "127.0.0.1",
            "root",
            "",
            "perpustakaan"
        );
        $query =
            "SELECT id, judul FROM buku WHERE judul LIKE '%$keyword%'";
        $result = mysqli_query($link, $query);
        while ($row = mysqli_fetch_array($result)) {
            $listbuku[] = $row;
        }
        mysqli_close($link);
        return $listbuku;
    }
    function display($listbuku)
    {
        echo "<br><table class='border-spacing-2 border-separate' style='width:100%'>";
        echo "<tr><th style='width:10%'>ID</th><th style='width:60%'> Judul </th><th></th></tr>";
        foreach ($listbuku as $row) {
            echo "<tr><td style='text-align: center;'>$row[0]</td><td> $row[1] </td><td style='text-align: center;'><a href='./pinjam/pinjam.php?fitur=add&idbuku=$row[0]&judul=$row[1]'><button class='px-2 py-1 bg-blue-900 text-white hover:bg-blue-950 transition-color duration-150 rounded-md'>pinjam</button></td></tr>";
        }
        echo "</table>";
    }
    ?>

    <form method=get class="h-8">
        <input type='text' name="keyword" class="focus:border-blue-700 border-[2px] px-1 h-full rounded-md outline-none " />
        <input type='submit' value="cari" class="bg-blue-700 text-white rounded-md px-5 h-full cursor-pointer hover:bg-blue-800" />
    </form>
    <a href='./pinjam/pinjam.php?fitur=read' class="underline decoration-blue-700 text-blue-800">Lihat Keranjang</a>
    <br>
</body>

</html>