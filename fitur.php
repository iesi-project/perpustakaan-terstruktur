<html>
<link rel="stylesheet" href="global.css">
<script src="https://cdn.tailwindcss.com"></script>

<body class="flex min-h-screen">
    <div class="h-fit w-fit m-auto bg-blue-200 py-5 px-7 rounded-lg">
        <?php
        include "cari.php";
        $fitur = $_GET['fitur'] ?? null;
        switch ($fitur) {
            case 'pinjam':
                header('location:pinjam/pinjam.php?fitur=read');
                exit;
            case 'pengembalian':
                header('location: pengembalian/pengembalian.php');
                exit;
            case 'cari':
            default:
                $keyword = $_GET['keyword'] ?? null;
                $listbuku = cari($keyword);
                display($listbuku);
                break;
        }
        ?>
    </div>
</body>

</html>