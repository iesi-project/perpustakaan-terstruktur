<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="global.css">
    <title>Perpustakaan</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="min-h-screen flex">
    <div class="container w-fit p-5 rounded-lg m-auto bg-blue-200">
        <h1 class="text-3xl font-semibold">Selamat datang di perpustakaan FILKOM</h1>
        <h3 class="text-xl">Layanan kami</h3>
        <form method="get" action="fitur.php" >
            <div class="flex gap-x-5 mt-4">
                <div>
                    <label for="radio_select_cari">Cari</label>
                    <input type="radio" id="radio_select_cari" checked name="fitur" value="cari">
                </div>
                <div>
                    <label for="radio_select_pinjam">Pinjam</label>
                    <input type="radio" id="radio_select_pinjam" name="fitur" value="pinjam">
                </div>
                <div>
                    <label for="radio_select_kembali">Pengembalian</label>
                    <input type="radio" id="radio_select_kembali" name="fitur" value="pengembalian">
                </div>
            </div>
            <button class="bg-blue-600 block mt-5 text-white py-1 px-6 rounded-lg hover:bg-blue-700 transition-all duration-200">Pilih</button>
        </form>
    </div>
</body>

</html>