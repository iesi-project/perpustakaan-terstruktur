<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pengembalian</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="min-h-screen flex">
    <?php
    include 'read-dipinjam.php';
    if ($result) {
        if ($result->num_rows > 0) {
            echo '<div class="m-auto"><table class="border-separate border-spacing-4 border-[1.5px] border-slate-400">';
            echo '<thead class="bg-slate-200 ">';
            echo '<tr>';
            echo '<th class="px-3 py-1.5">Judul</th>';
            echo '<th class="px-3 py-1.5">Tangggal Peminjaman</th>';
            echo '<th class="px-3 py-1.5">Aksi</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            while ($row = $result->fetch_assoc()) {
                echo '<tr>';
                echo '<td>' . $row['judul'] . '</td>';
                echo '<td>' . $row['tanggal'] . '</td>';
                echo "<td> <form method='POST' action='delete-dipinjam.php'><input type='hidden' name='buku_id' value=$row[buku_id] /> <input type='hidden' name='peminjaman_id' value=$row[peminjaman_id] /><input class='cursor-pointer py-1.5 px-3 bg-blue-900 hover:bg-blue-950 text-white rounded-md' type='submit' name='hapus_buku' value='Hapus'/></form></td>";
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table></div>';
        } else {
            echo  "<div class='m-auto flex flex-col items-center gap-y-4'><p class='text-xl font-semibold'>No records found.</p><img src='../assets/img2-.png' class='w-[18rem]' /></div>";
        }
        $result->free();
    } else {
        echo 'Error executing the query: ' . $link->error;
    }
    ?>
</body>

</html>